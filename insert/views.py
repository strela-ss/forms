from django.core.mail import EmailMessage
from django.http import HttpResponse, JsonResponse
from django.shortcuts import render
from django.views.generic import FormView
from insert.forms import ResumeForm, ContactForm


class ResumeFormView(FormView):
    form_class = ResumeForm

    def form_valid(self, form):
        data = form.cleaned_data
        email = EmailMessage(
            subject='Subject',
            body="Name: %s\nPhone: %s\nComments: %s" % (
                data["name"],
                data["phone"],
                data["comments"],
            ),
            # from_email=settings.EMAIL_HOST_USER,
            from_email="example@gmail.com",
            to=["hr@skillwillcorp.org", ],
        )
        if 'attachment' in self.request.FILES:
            file = self.request.FILES['attachment']
            email.attach(file.name, file.read(), file.content_type)
        email.send()
        return HttpResponse('success')

    def form_invalid(self, form):
        response = JsonResponse(form.errors)
        response.status_code = 400
        return response


class ContactFormView(FormView):
    form_class = ContactForm

    def form_valid(self, form):
        data = form.cleaned_data
        email = EmailMessage(
            subject='Subject',
            body="Name: %s\nEmail: %s\nMessage: %s" % (
                data["name"],
                data["email"],
                data["message"],
            ),
            # from_email=settings.EMAIL_HOST_USER,
            from_email="example@gmail.com",
            to=["hr@skillwillcorp.org", ],

        )
        email.send()
        return HttpResponse('success')

    def form_invalid(self, form):
        response = JsonResponse(form.errors)
        response.status_code = 400
        return response


def home(request):
    return render(request, "insert/home.html", {})
