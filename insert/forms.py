from django import forms

__author__ = 'Vladislav Strelnikov'


class ResumeForm(forms.Form):
    name = forms.CharField(required=True, max_length=64)
    phone = forms.CharField(max_length=13, required=False)
    comments = forms.CharField(widget=forms.TextInput(attrs={'cols': 35, 'rows': 6}), required=False)
    attachment = forms.FileField(required=False)


class ContactForm(forms.Form):
    name = forms.CharField(required=True, max_length=64)
    email = forms.EmailField(required=True)
    message = forms.CharField(widget=forms.TextInput(attrs={'cols': 35, 'rows': 6}))
