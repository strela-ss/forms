VALIDATIONS = {
  REQUIRED: 0,
  EMAIL: 1,
  FILE: 2
};
EXCEPTIONS = {
  REQUIRED: "This field is required.",
  EMAIL: "Enter a valid e-mail address.",
  FILE_TYPE: "Invalid file extention.",
  FILE_SIZE: "Invalid file size.",
  FORM: "There are errors on the form. Please fix them before continuing."
};


function generate_form_error(message) {
  error = $("<div></div>").addClass("form-button-error").append(
    $("<p></p>").text(message)
  );
  return error;
}
function generate_error(message) {
  error = $("<div></div>").addClass("form-error-message").append(
    $("<img>").attr("src", "/static/img/exclamation-octagon.png").css("margin-right", "5px").attr("align", "left")
  ).append(
    document.createTextNode(message)
  ).append(
    $("<div></div>").addClass("form-error-arrow").append(
      $("<div></div>").addClass("form-error-arrow-inner")
    )
  );
  return error;
}
function clear_errors(input) {
  input.parent().find(".form-error-message").each(function () {
    $(this).remove();
  })
}
function add_error(input, message) {
  input.parent().append(generate_error(message));
}
function isValidEmailAddress(emailAddress) {
  var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
  return pattern.test(emailAddress);
}
function validate(input, type) {
  if (input.parent().find(".form-error-message").length > 0) {
    return false;
  }
  switch (type) {
    case VALIDATIONS.REQUIRED:
      clear_errors(input);
      if (input.val().length === 0)
        add_error(input, EXCEPTIONS.REQUIRED);
      break;
    case VALIDATIONS.EMAIL:
      clear_errors(input);
      if (!isValidEmailAddress(input.val()))
        add_error(input, EXCEPTIONS.EMAIL);
      break;
    case VALIDATIONS.FILE:
      clear_errors(input);
      var ext = $(this).val().split('.').pop().toLowerCase();
      if ($.inArray(ext, ["pdf", "doc", "docx", "xls", "xlsx", "csv", "rtf", "html", "mpg", "flv", "avi", "jpg", "jpeg", "png", "gif"]) == -1)
        add_error($(this), EXCEPTIONS.FILE_TYPE);
      var size = this.files[0].size;
      if (size < 0 || size > 1024 * 1024)
        add_error($(this), EXCEPTIONS.FILE_SIZE);
      break;
    default:
  }
}
$("[class^='validate'], [class*=' validate'], :not([class*=' validate_upload'])").focus(function () {
  clear_errors($(this));
});
$(".validate_required").focusout(function () {
  validate($(this), VALIDATIONS.REQUIRED);
});
$(".validate_email").focusout(function () {
  validate($(this), VALIDATIONS.EMAIL);
});
$(".validate_upload").bind("change", function (e) {
  validate($(this), VALIDATIONS.FILE);
});

function toggle_popup() {
  $('.sw-mainarea').fadeToggle(200);
  $('.message').toggleClass('comein');
  $('.check').toggleClass('scaledown');
}

$(".form-submit-button").click(function () {
  $(this).parents("form").find(".validate_required").each(function () {
    validate($(this), VALIDATIONS.REQUIRED);
  });
  $(this).parent().parent().find(".form-button-error").each(function () {
    $(this).remove();
  });
  if ($(this).parents("form").find(".form-error-message").length > 0) {
    $(this).parent().parent().append(generate_form_error(EXCEPTIONS.FORM));
  } else {
    var self = $(this).parents("form");
    $.ajax({
      type: self.attr("method"),
      url: self.attr("action"),
      enctype: self.attr("enctype"),
      data: self.serialize(),
      success: function (data) {
        self[0].reset();
        toggle_popup();
      },
      error: function (data) {
        console.log(data);
        $.each(data.responseJSON, function (key, value) {
          add_error($("[name='" + key + "']"), value)
        });
      }
    })
  }
  return false;
});

$('#ok').click(function(){toggle_popup()});