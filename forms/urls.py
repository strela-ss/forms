from django.conf.urls import url
from django.contrib import admin
import insert.views as insert_view

urlpatterns = [
    url(r'^admin/', admin.site.urls),

    url(r'^resume-submit/$', insert_view.ResumeFormView.as_view(), name='resume_submit'),
    url(r'^contact-submit/$', insert_view.ContactFormView.as_view(), name='contact_submit'),

    url(r'^', insert_view.home),
]
